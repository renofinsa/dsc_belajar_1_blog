@extends('index')
@section('title','Ini Title')

@section('content')
    <h1>List Blog</h1>
    <hr>
    <div class="row">
        {{--  ini yang akan kita loop  --}}
        @foreach ($blog as $a)
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                <img style="height: 200px;" src="storage/upload/{{$a->gambar}}" alt="image">
                <div class="caption">
                    <a href="/detail/{{$a->id}}">
                        <h3>{{$a->judul}}</h3>
                    </a>

                    <p>{{$a->created_at->diffForHumans()}}</p>
                </div>
                </div>
            </div>
        @endforeach

        {{--  ini yang akan kita loop  --}}

    </div>
@endsection





