@extends('indexHalaman')
@section('title','Ini Title')

@section('content')

    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">{{$blog->judul}}</li>
    </ol>

    <hr>
    <div class="row">
        {{--  jadi kita tidak butuh di loop,
            karena sudah berdasarkan id  --}}
            <div class="col-sm-12 col-md-12">
                <div class="thumbnail">
                    <img style="width: 100%; height: 400px" src="../images/bg.png" alt="...">
                <div class="caption">
                    <h3>{{$blog->judul}}</h3>
                    <p>{{$blog->created_at->diffForHumans()}}</p>
                    <hr>
                <p>
                    {{$blog->isi}}
                </p>
                <hr>
                <h4>Comments</h4>
                <hr>
                </div>
                </div>
            </div>
        {{--  jadi kita tidak butuh di loop,
            karena sudah berdasarkan id  --}}


    </div>
@endsection





