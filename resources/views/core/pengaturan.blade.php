@extends('index')
@section('title','Pengaturan Blog')

@section('content')
        <div class="container">
            <h1>Pengaturan</h1>
        <hr>
        <div class="row">
            {{--  ini yang akan kita loop  --}}
            @foreach ($blog as $item)

                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img style="height: 200px;" src="storage/upload/{{$item->gambar}}" alt="image">
                        <div class="caption">
                            <h3>{{$item->judul}}</h3>
                            <p>08/11/2018</p>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href={{route('ubah', $item->id)}} class="btn btn-info form-control">Ubah</a>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-danger form-control" data-toggle="modal" data-target="#target{{$item->id}}">Hapus</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{--  ini modal hapus  --}}
                    <div id="target{{$item->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>apa kamu yakin ingin menghapus data <b>{{$item->judul}}</b>.</p>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form action={{route('hapus', $item->id) }} method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger form-control">Hapus</button>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-default form-control" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                            </div>

                        </div>
                    </div>
                {{--  ini modal hapus  --}}
            @endforeach

            {{--  ini yang akan kita loop  --}}



        </div>
@endsection





