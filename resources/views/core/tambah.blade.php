@extends('index')
@section('title','Ini Title')

@section('content')
    <h1>Tambah Blog</h1>
    <hr>
    <form action="{{route('addBlog')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="">Judul</label><br>
            <input name="judul" type="text" class="form-control" placeholder="buat judul kamu">
        </div>
        <div class="form-group">
            <label for="">Isi</label><br>
            <textarea name="isi" class="form-control"  id="" cols="5" rows="5"></textarea>
        </div>
        <div class="form-group">
            <label for="">Gambar</label><br>
            <input name="gambar" type="file" class="form-control" placeholder="buat judul kamu">
        </div>
        <div class="form-group">
            <Button type="submit" class="btn btn-info form-control">Kirim</Button>
        </div>
    </form>
@endsection





