@extends('indexHalaman')
@section('title','Ubah')

@section('content')
         <ol class="breadcrumb">
            <li><a href="/pengaturan">Pengaturan</a></li>
            <li class="active">Nama Blog</li>
        </ol>
        <hr>
        <form action={{route('proses', $blog->id)}} method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="PATCH">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <img style="width: 300px" src="../storage/upload/{{$blog->gambar}}" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Gambar</label><br>
                        <input name="gambar" name="gambar" type="file" class="form-control" >
                        <input name="gambar_lama" type="hidden" value="{{$blog->gambar}}" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">Judul</label><br>
                <input value="{{$blog->judul}}" name="judul" type="text" class="form-control" placeholder="buat judul kamu">
            </div>
            <div class="form-group">
                <label for="">Isi</label><br>
                <textarea class="form-control" name="isi" id="" cols="5" rows="5">
                        {{$blog->judul}}
                </textarea>
            </div>
            
            <div class="form-group">
                <Button class="btn btn-info form-control">Ubah</Button>
            </div>
        </form>
@endsection





