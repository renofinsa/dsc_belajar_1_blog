<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @yield('title')
    </title>

    {{-- ini bagian style atau css dari bootstrap --}}
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    {{-- ini bagian style atau css dari bootstrap --}}

  </head>
  <body>
    {{-- ini bagian content utama --}}
    {{-- ini bagian navbar --}}
        @include('partials.navbar')

    {{-- ini bagian navbar --}}
    {{-- ini bagian content utama --}}
    <div class="container">
        @yield('content')
    </div>
    {{-- ini bagian content utama --}}

    {{-- ini bagian jquery atau script --}}
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    {{-- ini bagian jquery atau script --}}
  </body>
</html>
