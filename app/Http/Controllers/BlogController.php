<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// kita harus tambahkan model
use App\Blog;
// kita harus tambahkan model
// package
// use Carbon\Carbon;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $blog = Blog::all();
        return view('core.lihat',compact('blog'));
    }

    public function pengaturan()
    {
        $blog = Blog::all();
        return view('core.pengaturan',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //handle upload image
        if($request->hasFile('gambar')){
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('gambar')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('gambar')->storeAs('public/upload', $file_store_name);
        }else{
            $file_store_name = 'noimage.jpg';
        }

        //kita akan membuat fungsi input data
        $newBlog = Blog::create([
            'judul' => request('judul'),
            'isi' => request('isi'),
            'gambar' => $file_store_name
        ]);

        return redirect('/');






        // $newBlog = new Blog;
        // $newBlog->judul = $request->get('judul');
        // $newBlog->isi = $request->get('isi');
        // $newBlog->gambar = $request->get('gambar');
        // $newBlog->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::find($id);
        return view('core.detail',compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('core.ubah',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $gambar_lama = $request->input('gambar_lama');
        
         //handle upload image
         if($request->hasFile('gambar')){
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('gambar')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('gambar')->storeAs('public/upload', $file_store_name);
            unlink('storage/upload/'.$gambar_lama);
        }else{
            $file_store_name = $gambar_lama;
        }

        $blog = Blog::find($id);
        $blog->judul = $request->get('judul');
        $blog->isi = $request->get('isi');
        $blog->gambar = $file_store_name;
        $blog->save();

        return redirect('/pengaturan-blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        unlink('storage/upload/'.$blog->gambar);
        $blog->delete();
        return redirect('/pengaturan-blog');
    }
}
