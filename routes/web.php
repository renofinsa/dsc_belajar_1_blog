<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//penggunaan route tanpa controller
Route::get('/tambah-blog', function () {
    return view('core.tambah');
});

//melihat semua data blog dari BlogController
Route::get('/','BlogController@index')->name('lihatSemuaBlog');

//menambah data blog dari BlogController
Route::post('/AddBlog','BlogController@store')->name('addBlog');

//melihat semua data blog dari BlogController pada view pengaturan
Route::get('/pengaturan-blog','BlogController@pengaturan');

//menghapus data blog dari BlogController berdasarkan ID
Route::delete('/hapus-blog/{id}','BlogController@destroy')->name('hapus');

//mengambil data blog dari BlogController berdasarkan ID
Route::get('/detail/{id}','BlogController@show')->name('detail');

//mengambil data blog dari BlogController berdasarkan ID
Route::get('/ubah/{id}','BlogController@edit')->name('ubah');

//merubah data blog dari BlogController berdasarkan ID
Route::patch('/proses/{id}','BlogController@update')->name('proses');
