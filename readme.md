<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## DSC Belajar Laravel
Disini kita akan membahas tentang basic dari framework Laravel
hingga menjadi sebuah Blog sederhana


## Minimal Pengetahuan
- PHP
- HTML+CSS

## Software yang digunakan
- XAMPP
- PHP 7.1++
- IDE ( Visual Studio Code / Atom)
- Browser ( Chrome/ Firefox)



## Cara Install
- install composer https://getcomposer.org/
- buka terminal/command prompt lalu ketik composer untuk testing apakah composer sudah terinstall
- install laravel, buka terminal/ command prompt lalu ketik "composer global require laravel/installer"
- clone project : git clone https://gitlab.com/renofinsa/dsc_belajar_1_blog.git
- buat database di XAMPP
- Setup koneksi database di file .ENV pada root laravel
- buka terminal/ command prompt lalu migrate database: php artisan migrate
- jika berhasil, buka terminal/ command prompt : php artisan serve
